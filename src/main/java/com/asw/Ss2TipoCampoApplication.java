package com.asw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss2TipoCampoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss2TipoCampoApplication.class, args);
	}
}

package com.asw;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Configuration
@PropertySource("application.yml")
@RestController
public class TypeController {



	@Value("${grounds}")
	private String grounds;

	@Value("${type}")
	private String type;




	@RequestMapping("/{campi}")
	public String getGrounds(@PathVariable String campi) {


		String[] campiArray = campi.split(",");
		String[] groundArray = grounds.split(",");
        String[] tipiArray = type.split(",");

        String result = "";
        
		for(int i = 0; i<campiArray.length-1; i++) {


			int tipo = (int) (Math.round(Math.random()*(1)));
			int ground = (int) (Math.round(Math.random()*(groundArray.length-1)));
			
			String tipoCampo = " " + campiArray[i] + " ( " + tipiArray[tipo] + " su " + groundArray[ground] + " ), ";


			result = result.concat(tipoCampo);

		}

		int tipo = (int) (Math.round(Math.random()*(1)));
		int ground = (int) (Math.round(Math.random()*(groundArray.length-1)));
		
		String tipoCampo = " " + campiArray[campiArray.length-1] + " ( " + tipiArray[tipo] + " su " + groundArray[ground] + " ). ";


		result = result.concat(tipoCampo);		
		
		return result;

	}


}
